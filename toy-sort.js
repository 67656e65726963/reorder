( function( $ ) {
	'use strict';

	const action = 'toy_sort';

	$(
		function() {
			const lineup = $( '#the-list' );

			const update = ( response ) => {
				if ( response === 're' ) {
					location.reload();

					return;
				}

				const { next } = $.parseJSON( response );

				if ( next ) {
					$.post(
						ajaxurl,
						{
							action,
							pointer: next.pointer,
							exclude: next.exclude,
							post_id: next[ 'post_id' ],
							prev_id: next[ 'prev_id' ],
							next_id: next[ 'next_id' ]
						},
						update
					);
				} else {
					$( '.re-ordering-row' ).removeClass( 're-ordering-row' );
					lineup.removeClass( 're-ordering' ).sortable( 'enable' );
				}
			}

			$( window ).load(
				function() {
					lineup.sortable(
						{
							axis: 'y',
							cancel: '.inline-edit-row',
							containment: 'table.widefat',
							cursor: 'move',
							distance: 2,
							helper: function( e, sorted ) {
								sorted.children().each(
									function( i, child ) {
										const w = $( child ).width();

										$( child ).width( w );
									}
								);

								return sorted;
							},
							items: '> tr',
							opacity: 0.8,
							start: function( e, sorted ) {
								if ( typeof inlineEditPost !== undefined ) {
									inlineEditPost.revert();
								}

								sorted.placeholder.height( sorted.item.height() );
							},
							stop: function( e, sorted ) {
								sorted.item.children().css( 'width', '' );
							},
							tolerance: 'pointer',
							update: function( e, sorted ) {
								const postId = sorted.item[ 0 ].id.substr( 5 );

								const prev   = sorted.item.prev();
								const prevId = prev.length ? prev.attr( 'id' ).substr( 5 ) : false;

								const next   = sorted.item.next();
								const nextId = next.length ? next.attr( 'id' ).substr( 5 ) : false;

								sorted.item.addClass( 're-ordering-row' );
								lineup.sortable( 'disable' ).addClass( 're-ordering' );

								$.post(
									ajaxurl,
									{
										action,
										post_id: postId,
										prev_id: prevId,
										next_id: nextId
									},
									update
								);

								$( 'tr.iedit' ).each(
									function( i, row ) {
										if ( i % 2 ) {
											$( row ).removeClass( 'alternate' );
										} else {
											$( row ).addClass( 'alternate' );
										}
									}
								);
							}
						}
					);
				}
			);
		}
	);
}( jQuery ) );
