<?php

/**
 * Plugin Name:       Toy Sort
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-sort
 * Description:       Helps put things in order using drag and drop.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_action(
	'load-edit.php',
	function() {
		$fold = get_current_screen();
		$type = $fold->post_type;

		if ( is_post_type_hierarchical( $type ) && post_type_supports( $type, 'page-attributes' ) ) {
			$name    = 'toy-sort';
			$version = '0.0.0';
			$path    = plugin_dir_url( __FILE__ );

			wp_enqueue_style( $name, $path . '/' . $name . '.css', null, $version );

			$deps = array( 'jquery-ui-sortable' );

			wp_enqueue_script( $name, $path . '/' . $name . '.js', $deps, $version, false );

			remove_action( 'pre_post_update', 'wp_save_post_revision' );
		}
	},
	67
);

add_action(
	'wp_ajax_toy_sort',
	function() {
		if ( empty( $_POST['post_id'] ) || ! isset( $_POST['prev_id'], $_POST['next_id'] ) ) {
			die( -1 );
		}

		$post = get_post( $_POST['post_id'] );

		if ( ! $post ) {
			die( -1 );
		}

		$post_id = $post->ID;

		$prev_id = empty( $_POST['prev_id'] ) ? false : (int) $_POST['prev_id'];
		$next_id = empty( $_POST['next_id'] ) ? false : (int) $_POST['next_id'];

		$pointer = empty( $_POST['pointer'] ) ? 1 : (int) $_POST['pointer'];

		$exclude = $_POST['exclude'];
		$exclude = empty( $exclude ) ? array( $post_id ) : array_filter( (array) $exclude, 'intval' );

		$backlog = array();
		$harvest = new stdClass();

		$harvest->next = false;

		$post_parent = $post->post_parent;
		$next_parent = $next_id ? wp_get_post_parent_id( $next_id ) : false;

		if ( $prev_id === $next_parent ) {
			$post_parent = $next_parent;
		} elseif ( $next_parent !== $post_parent ) {
			$prev_parent = $prev_id ? wp_get_post_parent_id( $prev_id ) : false;

			if ( $prev_parent !== $post_parent ) {
				$post_parent = $prev_parent || $next_parent;
			}
		}

		if ( $next_parent !== $post_parent ) {
			$next_id = false;
		}

		$statuses = get_post_stati( array( 'show_in_admin_all_list' => true ) );

		$siblings_query = array(
			'depth'                  => 1,
			'posts_per_page'         => get_option( 'posts_per_page', -1 ),
			'post_type'              => $post->post_type,
			'post_status'            => $statuses,
			'post_parent'            => $post_parent,
			'orderby'                => array(
				'menu_order' => 'ASC',
				'title'      => 'ASC',
			),
			'post__not_in'           => $exclude,
			'update_post_term_cache' => false,
			'update_post_meta_cache' => false,
			'suppress_filters'       => true,
			'ignore_sticky_posts'    => true,
		);

		$siblings = new WP_Query( $siblings_query );

		$ancestry       = get_post_ancestors( $post_id );
		$ancestry_depth = array( 'depth' => count( $ancestry ) );

		foreach ( $siblings->posts as $sibling ) {
			if ( $sibling->ID === $post_id ) {
				continue;
			}

			if ( $sibling->ID === $next_id ) {
				$options = array(
					'ID'          => $post_id,
					'menu_order'  => $pointer,
					'post_parent' => $post_parent,
				);

				wp_update_post( $options );

				$backlog[ $post_id ] = array_merge( $options, $ancestry_depth );
				$pointer++;
			}

			if ( $sibling->menu_order >= $pointer && isset( $backlog[ $post_id ] ) ) {
				break;
			}

			if ( $sibling->menu_order !== $pointer ) {
				$options = array(
					'ID'         => $sibling->ID,
					'menu_order' => $pointer,
				);

				wp_update_post( $options );
			}

			$backlog[ $sibling->ID ] = $pointer;
			$pointer++;

			if ( $sibling->ID === $prev_id && false === $next_id ) {
				$options = array(
					'ID'          => $post_id,
					'menu_order'  => $pointer,
					'post_parent' => $post_parent,
				);

				wp_update_post( $options );

				$backlog[ $post_id ] = array_merge( $options, $ancestry_depth );
				$pointer++;
			}
		}

		if ( false === $harvest->next && $siblings->max_num_pages > 1 ) {
			$harvest->next = array(
				'post_id' => $post->ID,
				'prev_id' => $prev_id,
				'next_id' => $next_id,
				'pointer' => $pointer,
				'exclude' => array_merge( array_keys( $backlog ), $exclude ),
			);
		}

		if ( false === $harvest->next ) {
			$options = array(
				'numberposts'            => 1,
				'post_type'              => $post->post_type,
				'post_status'            => $statuses,
				'post_parent'            => $post_id,
				'fields'                 => 'ids',
				'update_post_term_cache' => false,
				'update_post_meta_cache' => false,
			);

			$children = get_posts( $options );

			if ( ! empty( $children ) ) {
				die( 're' );
			}
		}

		$harvest->data = $backlog;

		die( wp_json_encode( $harvest ) );
	},
	67
);

add_filter(
	'pre_get_posts',
	function( $q ) {
		if ( $q->is_admin ) {
			$type = $q->query['post_type'];
			$pass = is_post_type_hierarchical( $type ) && post_type_supports( $type, 'page-attributes' );

			if ( $pass ) {
				$q->set( 'orderby', 'menu_order' );
				$q->set( 'order', 'ASC' );
			}
		}

		return $q;
	},
	67
);
