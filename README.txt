=== Toy Sort ===
Contributors: thewhodidthis
Tags: admin
Requires at least: 4.0.0
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Helps put things in order using drag and drop.
